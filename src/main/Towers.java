package main;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class Towers {
	
	private int size;
	
	private int CAN_MOVE = 1;				// Can move freely
	private int CAN_MOVE_TOWER_END = 2;		// Can move freely but reached the tower end 1st or 3rd
	private int CANNOT_MOVE_BLOCKED = -1;	// Stuck because a lower ring is blocking
	private int ERROR = -99;				// Simple error code
	
	private Deque<Ring> towerOne  = new ArrayDeque<Ring>();
	private Deque<Ring> towerTwo  = new ArrayDeque<Ring>();
	private Deque<Ring> towerThree  = new ArrayDeque<Ring>();
	private ArrayList<Deque<Ring>> towers = new ArrayList<Deque<Ring>>();
	
	public static void main(String[] args) {
		Towers towerProblem = new Towers(10);
		towerProblem.solve();
	}
	
	public Towers(int _size) {
		size = _size;

		for (int i = size; i > 0; i--) {
			towerOne.push(new Ring(i));
		}
		
		towers.add(towerOne);
		towers.add(towerTwo);
		towers.add(towerThree);		
	}
	
	public void solve() {
		
		int currentRingSize = 1; // Size 1 will be the smallest size regardless
		while (true) {
			// Reset if we've maxed out
			if (currentRingSize > size) {
				currentRingSize = 1;
			}
			
			// Get the current ring and its position
			Ring currentRing = null;
			int currentTower = -99;
			for (int i = 0; i < 3; i++) {
				Ring current = towers.get(i).peek();
				if (current != null) {								
					if (current.ringSize == currentRingSize) {
						currentRing = current;
						currentTower = i;
						break;
					}						
				}
			}
			
			// This just means we couldn't find it at the top of any stack. Meaning, our ring
			// is covered by somebody else
			if (currentRing == null) {
				currentRingSize++;
			}
			
			// However if we did find it, let's check our options
			if (currentRing != null) {
				
				// Based on its direction move it that way if possible. 
				int result = moveRing(currentTower, currentRing);
				
				if (result == ERROR) {
					break;
				} else if (result == CAN_MOVE_TOWER_END) {
					if (testForSucess()) {
						display();
						break;
					}
					currentRingSize++;
				} else if (result == CAN_MOVE) {
					// No increment, no success check. Left for demonstration purposes
				} else if (result == CANNOT_MOVE_BLOCKED) {
					currentRingSize++;
				}
			}
			
		}

	}
	
	public int moveRing(int currentTower, Ring ringRef) {
		int direction = ringRef.direction;
		int newTower = direction + currentTower;
		
		// Check for errors
		if (newTower < 0 || newTower > 2) {
			System.out.println("ILLEGAL STACK CHOICE");
			return ERROR;
		}
		
		Ring ringInNext = towers.get(newTower).peek();
		if (ringInNext == null || ringRef.ringSize < ringInNext.ringSize) {
			Ring ringToMove = towers.get(currentTower).pop();
			
			if (newTower == 0 || newTower == 2) {
				ringToMove.direction *= -1;
				towers.get(newTower).push(ringToMove);
				return CAN_MOVE_TOWER_END;
			} else {
				towers.get(newTower).push(ringToMove);
				return CAN_MOVE;
			}
		} else if (ringRef.ringSize > ringInNext.ringSize) {
			return CANNOT_MOVE_BLOCKED;
		} else {
			System.out.println("UNKNOWN ERROR OCCURRED");
			return ERROR;
		}
	}
	
	public boolean testForSucess() {		
		for (int i = 0; i < size; i++) {
			if (towerThree.size() != size) {
				return false;
			}
		}
		return true;
	}
	
	public void display() {
		for (int i = 0; i < size; i++) {
			Ring current = towers.get(2).pop();
			System.out.println(current.ringSize);
		}
	}

}
